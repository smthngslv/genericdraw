import os
import time
import numpy

from cv2 import cv2
from argparse import ArgumentParser
from multiprocessing import Pool
from multiprocessing.shared_memory import SharedMemory

def visualize(shape:tuple[int], data:numpy.ndarray) -> numpy.ndarray:
    result = numpy.zeros(shape, numpy.uint8)
    
    # Draw crosses.
    for (x, y, size, angle, b, g, r) in data:
        # Faster.
        x, y, size = int(x), int(y), int(size)
        
        # From degrees.
        angle = float(numpy.radians(angle))
        
        # BGR.
        color = (int(b), int(g), int(r))
        
        # Thickness of lines of the cross.
        thickness = max(size // 2 - 1, 1)
        
        # Size of affected area.
        box_radius = size + thickness
        
        # Affected area box.
        box_x0 = x - box_radius
        box_y0 = y - box_radius
        box_x1 = min(x + box_radius, shape[1] - 1)
        box_y1 = min(y + box_radius, shape[0] - 1)
        
        # Crop box.
        if box_x0 < 0:
            # Offset.
            x = box_radius + box_x0
            
            # Array index.
            box_x0 = 0
            
        else:
            x = box_radius
        
        # Crop box.
        if box_y0 < 0:
            # Offset.
            y = box_radius + box_y0
            
            # Array index.
            box_y0 = 0
            
        else:
            y = box_radius
            
        # Empty box.
        buffer = numpy.zeros((box_y1 - box_y0, box_x1 - box_x0, *shape[2:]), numpy.uint8)
        
        # Horizontal.
        x0 = x + int(numpy.cos(angle) * size)
        y0 = y + int(numpy.sin(angle) * size)
        
        angle_right = numpy.pi + angle
        x1 = x + int(numpy.cos(angle_right) * size)
        y1 = y + int(numpy.sin(angle_right) * size)
        
        # Draw in buffer.
        cv2.line(buffer, (x0, y0), (x1, y1), color, thickness)
        
        # Vertical.
        angle_left = numpy.pi / 2 + angle
        x0 = x + int(numpy.cos(angle_left) * size)
        y0 = y + int(numpy.sin(angle_left) * size)
        
        angle_right = 3 * numpy.pi / 2 + angle
        x1 = x + int(numpy.cos(angle_right) * size)
        y1 = y + int(numpy.sin(angle_right) * size)
        
        # Draw in buffer.
        cv2.line(buffer, (x0, y0), (x1, y1), color, thickness)
        
        # Affected area.
        image = result[box_y0:box_y1, box_x0:box_x1]
        
        # Masks.
        _, mask_image = cv2.threshold(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY_INV)
        _, mask_buffer = cv2.threshold(cv2.cvtColor(buffer, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY_INV)
        
        # Intersection.
        mask_blend = cv2.bitwise_or(mask_image, mask_buffer)
        
        # Different.
        result_diff = cv2.add(image, buffer, mask = mask_blend)
        # Intersection.
        result_inter = cv2.addWeighted(image, 0.5, buffer, 0.5, 0)
        
        # Remove difference from intersection.
        result_inter = cv2.bitwise_and(result_inter, result_inter, mask = cv2.bitwise_not(mask_blend))
        
        # Combine intersection and difference.
        result[box_y0:box_y1, box_x0:box_x1] = cv2.add(result_diff, result_inter)
        
    return result

def random_cross(shape:tuple[int], size:tuple[int]) -> numpy.ndarray:
    result = numpy.empty(7, numpy.uint16)
    
    # Position.
    result[0] = numpy.random.randint(0, shape[1], dtype = numpy.uint16)
    result[1] = numpy.random.randint(0, shape[0], dtype = numpy.uint16)
    
    # Size.
    result[2] = numpy.random.randint(*size, dtype = numpy.uint16)
    
    # Angle
    result[3] = numpy.random.randint(0, 90, dtype = numpy.uint16)
    
    # Color.
    result[4] = numpy.random.randint(0, 256, dtype = numpy.uint16)
    result[5] = numpy.random.randint(0, 256, dtype = numpy.uint16)
    result[6] = numpy.random.randint(0, 256, dtype = numpy.uint16)
    
    return result

def fitness(image:numpy.ndarray, reference:numpy.ndarray) -> float:
    # a - b
    difference = image.astype(numpy.float32) - reference
    
    # (a - b) ** 2
    squared_difference = numpy.square(difference)
    
    # Mean.
    mean_squared_difference = \
        numpy.sum(squared_difference) / numpy.product(image.shape)
    
    # Root mean square.
    return 1 - float(numpy.sqrt(mean_squared_difference)) / 255
    
def crossover(left:numpy.ndarray, right:numpy.ndarray, \
    shape:tuple[int], size:tuple[int], result:numpy.ndarray) -> None:
    
    for i in range(result.shape[0]):
        result[i] = left[i] if i & 1 else right[i]
        
    # Mutation.
    result[numpy.random.randint(0, result.shape[0])], result[-1] = \
        result[-1], random_cross(shape, size)

def generic_worker(data:tuple) -> numpy.ndarray:
    try:
        shape, info, size, population = data
        
        # SharedMemory.
        shared_memory_reference = SharedMemory('ReferenceImage')
        shared_memory_best = SharedMemory('BestIndividuals')
        
        # Load from shared memory.
        reference = numpy.ndarray(shape, numpy.uint8, shared_memory_reference.buf)
        best = numpy.ndarray(info, numpy.uint16, shared_memory_best.buf)
        
        # Population.
        individuals = numpy.empty((population, *best.shape[1:]), numpy.uint16)
        
        # Generate from best.
        for individual in individuals:
            left = best[numpy.random.randint(0, best.shape[0])]
            right = best[numpy.random.randint(0, best.shape[0])]
            
            # New.
            crossover(left, right, shape, size, individual)
        
        best_score = 0
        best_individual = None
        
        for individual in individuals:
            score = fitness(visualize(shape, individual), reference)
            
            # Find best.
            if score > best_score:
                best_score = score
                best_individual = individual
        
        return (best_score, best_individual)
        
    except KeyboardInterrupt:
        return None

def generic(reference:numpy.ndarray, size = (5, 20), generations = 2000, population = 100, \
    best_size = os.cpu_count(), crosses = 1500) -> numpy.ndarray:
    
    # Info.
    print('Generation start with:')
    print(f'\t{generations} generations.')
    print(f'\t{population} population.')
    print(f'\t{crosses} crosses of size between {size}.')
    print(f'\tWith {best_size} samples (threads).')
    
    # Info for workers.
    info = (best_size, crosses, 7)
    
    # Allocate shared memory.
    shared_memory_reference = SharedMemory('ReferenceImage', True, reference.nbytes)
    shared_memory_best = SharedMemory('BestIndividuals', True, int(numpy.product(info)) * 2)
    
    # Make global.
    individual = None
    
    try:    
        shared_reference = numpy.ndarray(reference.shape, numpy.uint8, shared_memory_reference.buf)
        shared_best = numpy.ndarray(info, numpy.uint16, shared_memory_best.buf)
        
        # Copy reference to the shared memory.
        shared_reference[:] = reference
            
        # Random population.
        for individual in shared_best:
            for cross in individual:
                cross[:] = random_cross(reference.shape, size)
        
        # Fitness for the best individuals.
        scored = [fitness(visualize(reference.shape, i), reference) for i in shared_best]
        
        # Data to provide to the workers.
        workers_data = [(reference.shape, info, size, population // info[0]), ] * info[0]
        
        # Start time.
        t = time.time()
        
        # Processes pool.
        with Pool(processes = info[0]) as pool:
            for generation in range(generations):
                # In parallel.
                new_best = [i for i in pool.imap_unordered(generic_worker, workers_data)]
                
                # Add old best to the new.
                new_best += [i for i in zip(scored, shared_best)]
                    
                # Sort by fitness.
                new_best.sort(key = lambda x:x[0])
                    
                # Take best.
                for (i, (score, individual)) in zip(range(info[0]), new_best[info[0]:]):
                    scored[i] = score
                    shared_best[i] = individual
                
                # Latest image.
                latest = visualize(reference.shape, individual)
                
                # Show image.
                cv2.imshow('Best', latest)
                cv2.imwrite('.tmp.png', latest)
                cv2.waitKey(1)
                
                print(f'Gen: {generation + 1}/{generations}. Similarity: {round(score * 100, 2)}%. Time: {int(time.time() - t)} s.')
            
    except KeyboardInterrupt:
        pass
        
    finally:
        # Free memory.
        shared_memory_reference.close()
        shared_memory_reference.unlink()
        shared_memory_best.close()
        shared_memory_best.unlink()
        
        return visualize(reference.shape, individual)
    
if __name__ == '__main__':
    parser = ArgumentParser(description = 'Generic image generator.')
    
    # Obligatory.
    parser.add_argument('input_path', \
        type = str, help = 'The path to the input image.')
    
    # To save the image.
    parser.add_argument('-o', dest = 'output_path', \
        type = str, help = 'The path to the output image.', default = None)
    
    # Size.
    parser.add_argument('-s', dest = 'min_size', \
        type = int, help = 'The minimum size of the cross.', default = 5)
    parser.add_argument('-S', dest = 'max_size', \
        type = int, help = 'The maximum size of the cross.', default = 20)
    
    # Generic.
    parser.add_argument('-g', dest = 'generations', \
        type = int, help = 'The generations count.', default = 2000)
    parser.add_argument('-p', dest = 'population', \
        type = int, help = 'The population size.', default = 100)
    parser.add_argument('-b', dest = 'best_size', \
        type = int, help = 'The sample size of the population.', default = os.cpu_count())
    
    # Other.
    parser.add_argument('-c', dest = 'crosses', \
        type = int, help = 'The number of the crosses.', default = 1500)
    
    # Parse.
    args = parser.parse_args()
    
    assert os.path.isfile(args.input_path), \
        f'File does not exists: "{args.input_path}".'
    
    # Generate.
    result = generic(cv2.imread(args.input_path), (args.min_size, args.max_size), \
        args.generations, args.population, args.best_size, args.crosses)
    
    if args.output_path:
        cv2.imwrite(args.output_path, result)
        
    # Exit.
    cv2.waitKey(0)
    cv2.destroyAllWindows()