Using
===

1. Install `Python 3.9`.

2. Install `requirements.txt`:
```bash=
pip install -r requirements.txt
```

3. Run:
```bash=
python generate.py PATH_TO_IMAGE
```

4. To see the help run:
```bash=
python generate.py -h
```

5. Help.
```bash=
usage: generate.py [-h] [-o OUTPUT_PATH] [-s MIN_SIZE] [-S MAX_SIZE]
                   [-g GENERATIONS] [-p POPULATION] [-b BEST_SIZE]
                   [-c CROSSES]
                   input_path

Generic image generator.

positional arguments:
  input_path      The path to the input image.

optional arguments:
  -h, --help      show this help message and exit
  -o OUTPUT_PATH  The path to the output image.
  -s MIN_SIZE     The minimum size of the cross.
  -S MAX_SIZE     The maximum size of the cross.
  -g GENERATIONS  The generations count.
  -p POPULATION   The population size.
  -b BEST_SIZE    The sample size of the population.
  -c CROSSES      The number of the crosses.
```

6. Examples.

* Run with default setting without saving:
```bash=
python generate.py my_image.png
```

* Run with default setting with saving:
```bash=
python generate.py my_image.png -o saved_image.png
```

* Run with big crosses and saving:
```bash=
python generate.py my_image.png -o saved_image.png -s 10 -S 30
```
